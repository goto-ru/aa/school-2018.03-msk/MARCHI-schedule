import json
from collections import Iterable
from random import randint
import re


class Interpreter:
    stack = list()
    grammars = dict()

    # тут лежит словарь из правил для каждой грамматики, по которым нужно перейти, чтобы получить терминал (TERM: №)
    terminals_dict = dict()

    def __init__(self, memory_block):
        self.memory_block = memory_block
        self.start()

    def start(self):

        with open('grammars.json', 'r', encoding='utf-8') as file:
            self.grammars = json.load(file)

        for gr in self.grammars.keys():
            self.terminals_dict[gr] = dict()
            grammar = self.grammars[gr]
            terminals = self.create_terminals_list(grammar)
            for nterm in grammar.keys():
                self.terminals_dict[gr][nterm] = dict()

                #  очищаем temp_memory
                temp_memory = [nterm, None]
                self.preprocess(nterm, grammar, temp_memory)

                #  temp_memory для каждого нетерминала:
                #  [нетерминал, элемент который был нужен в preprocessing, [терминал, номер правила], [--=--], [] ...]
                #  переносим данные из temp_memory в terminals_dict
                for i in range(2, len(temp_memory)):
                    self.terminals_dict[gr][nterm][temp_memory[i][0]] = temp_memory[i][1]
                for i in terminals:
                    if i not in self.terminals_dict[gr][nterm].keys():
                        self.terminals_dict[gr][nterm] = dict(self.terminals_dict[gr][nterm], **{i: -1})
                        self.terminals_dict[gr][nterm].update()

    def recognize(self, message, username):
        """
        Основа пониматора: возвращает название команды, если распознала её, и False, если нет
        """
        for gr in self.grammars.keys():
            self.stack = list()

            # словарь правил, по которым пониматор ищет терминалы для данной грамматики
            terminals_dict = self.terminals_dict[gr]

            # список из терминалов для данной грамматики
            terminals_list = self.create_terminals_list(self.grammars[gr])

            self.stack.append('<COMMAND>')
            for sym in re.split(r'\s|;|:|,|\.', message):
                if sym not in terminals_list:
                    continue
                while sym != self.stack[-1]:
                    try:
                        # тут лежит(ат) элемент(ы) на который(е) мы меняем последний элемент в стеке
                        change = self.grammars[gr][self.stack[-1]]
                        change = change[terminals_dict[self.stack[-1]][sym]]
                        change = change.split('&')[::-1]
                        print(self.stack[-1], change)
                    except KeyError:
                        continue
                    self.stack[-1] = change
                    self.stack = list(self.flatten(self.stack))
                self.stack.remove(sym)
            if len(self.stack) == 0:
                self.memory_block.cache.append({username: gr})
                return
        self.memory_block.cache.append({username: None})

    def flatten(self, items):
        for x in items:
            if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
                yield from self.flatten(x)
            else:
                yield x

    def preprocess(self, symbol, grammar, temp_memory):
        for i in range(len(grammar[symbol])):
            if temp_memory[0] is symbol:
                temp_memory[1] = i
            if not self.is_terminal(grammar[symbol][i]):
                sym = grammar[symbol][i].split('&')[0]
                self.preprocess(sym, grammar, temp_memory)
            else:
                action = [grammar[symbol][i], temp_memory[1]]
                temp_memory.append(action)

    def create_terminals_list(self, grammar):
        terminals_list = list()
        for key in grammar.keys():
            for sym in grammar[key]:
                if self.is_terminal(sym):
                    terminals_list.append(sym)
        return terminals_list

    @staticmethod
    def is_terminal(symbol):
        return False if '<' in symbol or '>' in symbol else True


class Generator:
    grammars_gen = dict()

    def __init__(self, memory_block):
        self.memory_block = memory_block
        with open('grammars_gen.json', 'r', encoding='utf-8') as file:
            self.grammars_gen = json.load(file)

    def generate(self):
        username = list(self.memory_block.cache[-1].keys())[0]
        if list(self.memory_block.cache[-1].values())[0] is None:
            return None, username, None
        else:
            grammar = self.grammars_gen[list(self.memory_block.cache[-1].values())[0]]
            keys = list(grammar.keys())
            command = list(self.memory_block.cache[-1].values())[0]
            answer = self.create_answer(grammar, keys)
            return answer, username, command

    @staticmethod
    def create_answer(grammar, keys):
        keys = list(keys)
        if keys == ['']:
            return ''
        else:
            c = ''
            r = randint(0, len(grammar[keys[0]]) - 1)
            a = grammar[keys[0]][r]
            for i in a:
                for k in i.split():
                    if '<' not in k:
                        c = c + ' ' + k
                    elif Generator.create_answer(grammar, [k]) == '':
                        c = c + k
                    else:
                        c = c + Generator.create_answer(grammar, [k])
            return c


class GrammarMemory:
    memor = []

    def __init__(self):
        with open('schedule.txt', 'rt') as file:
            self.cache = list()
            self.schedule = file.read()
            self.schedule = self.schedule.split('\n')
        self.start()

    def start(self):
        for i in range(len(self.schedule)):
            self.memor.append([])
            for k in self.schedule[i].split(';'):
                self.memor[i].append(k)

    def group(self, number):
        gr1 = None
        for i in range(len(self.memor)):
            if self.memor[i] == number:
                gr1 = i
                break
        gr2 = len(self.memor) - 1
        for i in range(gr1, len(self.memor)):
            if self.memor[i] == number + 1:
                gr2 = i
                break
        return gr1, gr2

    def date(self, number):
        if 'all' in number.split():
            all_date = []
            for i in range(self.group(number.split()[1])[0], self.group(number.split()[1])[1] + 1):
                all_date.append(self.memor[i][1])
            return all_date
        else:
            return self.memor[number - 1][1]

    def time(self, number):
        if 'all' in number.split():
            all_time = []
            for i in range(self.group(number.split()[1])[0], self.group(number.split()[1])[1] + 1):
                all_time.append(self.memor[i][2])
            return all_time
        else:
            return self.memor[number - 1][2]

    def lesson(self, number):
        if 'all' in number.split():
            all_lessons = []
            for i in range(self.group(number.split()[1])[0], self.group(number.split()[1])[1] + 1):
                all_lessons.append(self.memor[i][3])
            return all_lessons
        else:
            return self.memor[number - 1][3]

    def cabinet(self, number):
        if 'all' in number.split():
            all_cabinets = []
            for i in range(self.group(number.split()[1])[0], self.group(number.split()[1])[1] + 1):
                all_cabinets.append(self.memor[i][4])
            return all_cabinets
        else:
            return self.memor[number - 1][4]
