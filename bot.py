import os

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from config import token
import logging
from logic import Interpreter, Generator, GrammarMemory

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

PORT = int(os.environ.get('PORT', '8443'))

memory_block = GrammarMemory()
interpreter = Interpreter(memory_block)
generator = Generator(memory_block)


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"', update, error)




def start(bot, update):
    update.message.reply_text('Привет')


def message_handler(bot, update):
    username = update.message.chat_id
    interpreter.recognize(update.message.text, username)
    answer, chat_id, command = generator.generate()
    memory_block.cache.remove({chat_id: command})
    if answer is None:
        answer = 'Я ничего не понял'
    bot.send_message(chat_id, answer)


def main():
    updater = Updater(token)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(MessageHandler(Filters.text, message_handler))

    dp.add_error_handler(error)

    print("Started!")
    """updater.start_webhook(listen='0.0.0.0',
                          port=PORT,
                          url_path=token)
    updater.bot.set_webhook('https://ours-quote-bot.herokuapp.com/' + token)"""
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
